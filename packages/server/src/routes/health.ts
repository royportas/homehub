import { FastifyInstance, RouteOptions } from "fastify";

export default (fastify: FastifyInstance, opts: any, done: () => void) => {
  // Simple health check, extend once we have MQTT connections
  fastify.get("/", async () => {
    return { statusCode: 200, status: "ok" };
  });

  done();
};
