import fastify from "fastify";
import fastifyStatic from "fastify-static";
import pino from "pino";
import path from "path";
import health from "./routes/health";
import { mqttPlugin } from "./modules/mqtt";

// Environment variables, replace with .env
const PORT = 3001;
const BROKER_URL = "mqtt://192.168.1.2";

const logger = pino({ level: "info" });

const server = fastify({ logger: logger });

// Add plugins
server.register(fastifyStatic, {
  root: path.join(__dirname, "..", "..", "frontend", "build"),
});

// Define routes
server.register(health, { prefix: "/health" });

// Setup the modules
server.register(mqttPlugin, {
  brokerUrl: BROKER_URL,
  logger: logger.child({ module: "mqtt" }),
});

server.get("/devices", function (request, reply) {
  const mqtt = this.mqtt;
  reply.send(mqtt.getDevices());
});

// Run the server
const start = async () => {
  try {
    await server.listen(PORT);
  } catch (err) {
    server.log.error(err);
    process.exit(1);
  }
};
start();
