import Mqtt from "../../modules/mqtt";

declare module "fastify" {
  interface FastifyInstance {
    mqtt: Mqtt;
  }
}
