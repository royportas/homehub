# HomeHub

[![lerna](https://img.shields.io/badge/maintained%20with-lerna-cc00ff.svg)](https://lerna.js.org/)
[![pipeline status](https://gitlab.com/royportas/homehub/badges/master/pipeline.svg)](https://gitlab.com/royportas/homehub/-/commits/master)
[![coverage report](https://gitlab.com/royportas/homehub/badges/master/coverage.svg)](https://gitlab.com/royportas/homehub/-/commits/master)

HomeHub is a modular home automation tool written in NodeJS.
Its designed to be the "glue" between other Home Automation tools (such as Home Assistant, Hue, MQTT, etc.)

## What it does?

This is fairly specific to my needs, but its designed in a way that stuff can be easily added onto it. For now, it does the following:

- Hue Dimmer Switch to HomeAssistant (via MQTT)
- lm-sensors HomeAssistant sensor (via MQTT)
- Air Quality

Over time more stuff will probably be added

## Hosting

This is designed to be hosted as a docker container

## Building

This project is managed by Lerna, all the modules can be built using `lerna run build`
