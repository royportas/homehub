/**
 * https://www.home-assistant.io/docs/mqtt/discovery/#examples
 */
interface DeviceConfig {
  name: string;
  state_topic: string;
}

export class BinarySensor {}

export class HomeAssistant {
  public getDevices() {}
}
