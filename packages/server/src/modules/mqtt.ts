import mqtt from "mqtt";
import pino from "pino";
import fastifyPlugin from "fastify-plugin";

/**
 * Abstraction around the MQTT.js client
 */
export default class Mqtt {
  private client?: mqtt.MqttClient;
  private brokerUrl: string;
  private logger: pino.Logger;

  constructor(brokerUrl: string, logger: pino.Logger) {
    this.brokerUrl = brokerUrl;
    this.logger = logger;
    // this.client = mqtt.connect(brokerUrl);
    this.logger.info(`Connecting to ${brokerUrl}`);
  }

  public async connect() {
    return new Promise((resolve, reject) => {
      this.client = mqtt.connect(this.brokerUrl);
      this.client.on("connect", () => {
        this.logger.info("Successfully connected to broker");
        resolve();
      });
    });
  }

  public getDevices() {
    return [
      {
        name: "Test device",
      },
    ];
  }
}

export const mqttPlugin = fastifyPlugin(async (fastify, options) => {
  const brokerUrl = options.brokerUrl;
  delete options.brokerUrl;
  const logger = options.logger;
  delete options.logger;

  const mqtt = new Mqtt(brokerUrl, logger);
  await mqtt.connect();

  fastify.decorate("mqtt", mqtt);
});
